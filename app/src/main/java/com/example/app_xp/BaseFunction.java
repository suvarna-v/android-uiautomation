package com.example.app_xp;

public interface BaseFunction {

    String Voot_Package = "com.tv.v18.viola";

    void launchApp(String appPackage);
    void closeApp(String packageName);
    void sleep(long timeout);
    void captureEvent(String eventName, String eventValue);
    void captureEvent(String eventName, EventLogger eventValue);
}
