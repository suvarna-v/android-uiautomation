package com.example.app_xp;

import java.io.Serializable;

public class KpiModelClass implements Serializable {

    public static String launchTime = null;
    public static String SearchTime = null;
    public static String enterSearch = null;

    public static String getLaunchTime() {
        return launchTime;
    }

    public static void setLaunchTime(String launchTime) {
        KpiModelClass.launchTime = launchTime;
    }

    public static String getSearchTime() {
        return SearchTime;
    }

    public static void setSearchTime(String searchTime) {
        SearchTime = searchTime;
    }

    public static String getEnterSearch() {
        return enterSearch;
    }

    public static void setEnterSearch(String enterSearch) {
        KpiModelClass.enterSearch = enterSearch;
    }

}
