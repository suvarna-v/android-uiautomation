package com.example.app_xp.interfaces;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface UpdateDeviceStatus {
    String deviceId() default "";
    String ipAddress() default "";
    String deviceStatus() default "";
}
