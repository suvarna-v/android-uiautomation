package com.example.app_xp.delegate;

import android.content.Context;

import androidx.annotation.Nullable;

import com.example.app_xp.interfaces.UpdateDeviceStatus;

public class EventLoggingDelegateImpl extends EventLoggingDelegate {
    private Class mTestClass;
    private Context mContext;
    private FindAnnotatedElementDelegate mFindElement;

    public EventLoggingDelegateImpl(Class testClass, Context context) {
        this.mTestClass = testClass;
        this.mContext = context;
        mFindElement = FindAnnotatedElementDelegate.create(mTestClass, mContext);
    }

    @UpdateDeviceStatus
    @Nullable
    @Override
    public void updateDeviceStatus(boolean isStart) {
        try {
            mFindElement.findElement(isStart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
