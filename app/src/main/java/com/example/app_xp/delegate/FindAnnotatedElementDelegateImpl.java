package com.example.app_xp.delegate;

import android.content.Context;

import com.example.app_xp.interfaces.UpdateDeviceStatus;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class FindAnnotatedElementDelegateImpl extends FindAnnotatedElementDelegate {
    private Class mTestClass;
    private Context mContext;

    public FindAnnotatedElementDelegateImpl(Class testClass, Context context) {
        this.mTestClass = testClass;
        this.mContext = context;
    }

    @UpdateDeviceStatus
    @Override
    public void findElement(boolean isStart) {
        try {
            if (mTestClass != null) {
                throw new IllegalStateException(
                        "Test Class is not available.");
            }
            Method element;
            if (isStart) {
                element = mTestClass.getMethod("startApplication", new Class[]{});
            } else {
                element = mTestClass.getMethod("closeApplication", new Class[]{});
            }
            System.out.println("\n Finding annotations on " + element.getClass().getName());
            Annotation[] annotations = element.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof UpdateDeviceStatus) {
                    UpdateDeviceStatus fileInfo = (UpdateDeviceStatus) annotation;
                    System.out.println("DeviceId :" + fileInfo.deviceId());
                    System.out.println("IpAderess :" + fileInfo.ipAddress());
                    System.out.println("deviceStatus :" + fileInfo.deviceStatus());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
