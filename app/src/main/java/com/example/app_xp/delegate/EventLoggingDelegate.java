package com.example.app_xp.delegate;

import android.content.Context;

import androidx.annotation.Nullable;

public abstract class EventLoggingDelegate {
    static final String TAG = "EventLoggingDelegate";
    /**
     * Private constructor
     */
    EventLoggingDelegate() {}

    public static EventLoggingDelegate create(Class testClass, Context context) {
        return new EventLoggingDelegateImpl(testClass, context);
    }

    /**
     * @return AppCompat's action bar, or null if it does not have one.
     */
    @Nullable
    public abstract void updateDeviceStatus(boolean isStart);
}
