package com.example.app_xp.delegate;

import android.content.Context;

public abstract class FindAnnotatedElementDelegate {
    public static FindAnnotatedElementDelegate create(Class testClass, Context context) {
        return new FindAnnotatedElementDelegateImpl(testClass, context);
    }
    public abstract void findElement(boolean isStart);
}
