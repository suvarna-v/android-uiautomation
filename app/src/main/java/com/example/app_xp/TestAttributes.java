package com.example.app_xp;
import java.sql.Timestamp;

public class TestAttributes {
    String testId;
    String job_id;
    String device_id;
    String ipAdress;
    String location;
    String order;
    String script;
    String appName;
    String startDate;
    boolean isVcap;
    boolean isPcap;
    String applicationUnderTestName;
    String applicationUnderTestVersion;
    String deviceStatus;
    boolean isLiveStreaming;

}
