package com.example.app_xp;

import java.io.Serializable;

public class EventLogger extends TestAttributes implements Serializable {

    String testRunId;
    String date;
    String timeStamp;
    String eventName;
    String elementToBeSearched;
    String actionPerformed;

    public String getTestRunId() {
        return testRunId;
    }

    public void setTestRunId(String testRunId) {
        this.testRunId = testRunId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
