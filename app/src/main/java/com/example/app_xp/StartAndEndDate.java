package com.example.app_xp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StartAndEndDate {

        @SerializedName("TestStartTime")
        @Expose
        String startdDateTime = "";

        @SerializedName("TestEndTime")
        @Expose
        String endDateTime = "";

        @SerializedName("PCapStartTime")
        @Expose
        String pCapStartDateTime = "";

        @SerializedName("PCapEndTime")
        @Expose
        String pCapEndDateTime = "";

        @SerializedName("VideoStartTime")
        @Expose
        String vCapStartDateTime = "";

        @SerializedName("VideoEndTime")
        @Expose
        String vCapEndDateTime = "";

        private String timeZoneDisplay;
        private String timeZoneId;

        public StartAndEndDate(
                String startdDateTime,
                String endDateTime,
                String pCapStartDateTime,
                String pCapEndDateTime,
                String vCapStartDateTime,
                String vCapEndDateTime,
                String timeZoneDisplay,
                String timeZoneId) {
            this.startdDateTime = startdDateTime;
            this.endDateTime = endDateTime;
            this.pCapStartDateTime = pCapStartDateTime;
            this.pCapEndDateTime = pCapEndDateTime;
            this.vCapStartDateTime = vCapStartDateTime;
            this.vCapEndDateTime = vCapEndDateTime;
            this.timeZoneDisplay = timeZoneDisplay;
            this.timeZoneId = timeZoneId;
        }

        public String getStartdDateTime() {
            return startdDateTime;
        }

        public void setStartdDateTime(String startdDateTime) {
            this.startdDateTime = startdDateTime;
        }

        public String getEndDateTime() {
            return endDateTime;
        }

        public void setEndDateTime(String endDateTime) {
            this.endDateTime = endDateTime;
        }

        public String getpCapStartDateTime() {
            return pCapStartDateTime;
        }

        public void setpCapStartDateTime(String pCapStartDateTime) {
            this.pCapStartDateTime = pCapStartDateTime;
        }

        public String getpCapEndDateTime() {
            return pCapEndDateTime;
        }

        public void setpCapEndDateTime(String pCapEndDateTime) {
            this.pCapEndDateTime = pCapEndDateTime;
        }

        public String getvCapStartDateTime() {
            return vCapStartDateTime;
        }

        public void setvCapStartDateTime(String vCapStartDateTime) {
            this.vCapStartDateTime = vCapStartDateTime;
        }

        public String getvCapEndDateTime() {
            return vCapEndDateTime;
        }

        public void setvCapEndDateTime(String vCapEndDateTime) {
            this.vCapEndDateTime = vCapEndDateTime;
        }
}
