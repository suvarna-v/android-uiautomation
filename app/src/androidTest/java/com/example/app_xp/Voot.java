package com.example.app_xp;

import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import com.example.app_xp.app.BaseApp;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.sql.Timestamp;
import java.util.Date;

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Voot extends BaseApp {
    long OneMin_Timeout = 60000;
    boolean value = false;
    String searchButtonID = "Search";
    String enterSearchID = "com.tv.v18.viola:id/fr_search_edit";
    String searchText = "Big boss season 13 episode 2 \n ";
    String searchVideo = "My Voot";
    String loaderID = "com.tv.v18.viola:id/progress_loader";
    String playid = "com.tv.v18.viola:id/vh_iv_play_button";
    String videoID = "com.tv.v18.viola:id/vh_iv_play_button";
    String elementId = "com.tv.v18.viola:id/search_grid";
    String elementIdAfterSearch = "com.tv.v18.viola:id/frag_rv_list";

    public Voot(Class testClass) {
        super(testClass);
    }

    @Before
    public void startAppActivity() {
        try {
            launchApp(BaseFunction.Voot_Package);
            captureEvent("appLaunch", BaseFunction.Voot_Package);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void a_clickOnSearchButton() {
        try {
            UiSelector eventId = new UiSelector().descriptionContains(searchButtonID);
            UiObject eventAppear = device.findObject(eventId);
            if (eventAppear.waitForExists(OneMin_Timeout)) {
                eventAppear.click();
                captureEvent("clickEvent", searchButtonID);
                value = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void b_enterOnSearchButton() {
        try {
            UiSelector enterSearchField = new UiSelector().resourceId(enterSearchID);
            UiObject enterSearch_text = device.findObject(enterSearchField);
            if (enterSearch_text.waitForExists(OneMin_Timeout)) {
                enterSearch_text.setText(searchText);
                captureEvent("enterSearchText", enterSearchID);
                value = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void c_SearchElementId() {
        try {
            UiSelector ElementsID = new UiSelector().resourceId(elementId);
            UiObject Elements = device.findObject(ElementsID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (Elements.exists() || Elements.isEnabled()) {
                    captureEvent("HomeElementApear", "Yes");
                    break;
                } else {
                    captureEvent("HomeElementApear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void d_clickOnSearchButton() {
        try {
            UiSelector eventId = new UiSelector().descriptionContains(searchVideo);
            UiObject eventAppear = device.findObject(eventId);
            if (eventAppear.waitForExists(OneMin_Timeout)) {
                eventAppear.click();
                captureEvent("clickEvent", searchVideo);
                value = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void e_SearchElementId() {
        try {
            UiSelector ElementsID = new UiSelector().resourceId(elementIdAfterSearch);
            UiObject Elements = device.findObject(ElementsID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (Elements.exists() || Elements.isEnabled()) {
                    captureEvent("HomeElementApear", "Yes");
                    break;
                } else {
                    captureEvent("HomeElementApear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void f_play() {
        try {
            UiSelector playId = new UiSelector().resourceId(playid);
            UiObject playBtn = device.findObject(playId);

            UiSelector videoCardID = new UiSelector().resourceId(videoID);
            UiObject videoCard = device.findObject(videoCardID);

            if (playBtn.waitForExists(OneMin_Timeout) && playBtn.isClickable()) {
                playBtn.click();
                captureEvent("playVideo", playid);
                value = true;
            } else {
                videoCard.swipeLeft(1);
            }
        } catch (Exception e) {
         e.printStackTrace();
        }
    }

    @Test
    public void g_bufferReadWithID() {
        try {
            UiSelector loaderId = new UiSelector().resourceId(loaderID);
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (loader.exists()) {
                    captureEvent("loaderAppear", "Yes");
                } else {
                    captureEvent("loaderAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void close() {
        try {
            closeApp(BaseFunction.Voot_Package);
            captureEvent("appKilled", BaseFunction.Voot_Package);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
