package com.example.app_xp;

import android.util.Log;

import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import java.sql.Timestamp;
import java.util.Date;

public class BaseMethod {

    int OneMin_Timeout = 60000;
    Timestamp timestamp = null;
    UiSelector selector = null;
    UiObject idObject = null;

    public boolean clickEvent(UiDevice device, String elementID, String idType, String elementName, String successMsg) {
        boolean result = false;
        try {
            if (idType.equalsIgnoreCase("resourceID")) {
                selector = new UiSelector().resourceId(elementID);
            }
            if (idType.equalsIgnoreCase("textContains")) {
                selector = new UiSelector().textContains(elementID);
            }
            if (idType.equalsIgnoreCase("text")) {
                selector = new UiSelector().text(elementID);
            }
            if (idType.equalsIgnoreCase("description")) {
                selector = new UiSelector().description(elementID);
            }
            if (idType.equalsIgnoreCase("descriptionContains")) {
                selector = new UiSelector().descriptionContains(elementID);
            }
            return click(device,selector,elementName,successMsg);
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }
    public boolean click(UiDevice device,UiSelector selector,String elementName,String successMsg){
        boolean result = false;
        try {
            Log.d("Aquamark", "Trying To Execute " + elementName);
            idObject = device.findObject(selector);
            if (idObject.waitForExists(OneMin_Timeout)) {
                idObject.click();
                timestamp = new Timestamp(new Date().getTime());
                Log.d("Aquamark", successMsg + timestamp);
                result = true;
            }
            return result;
        } catch (Exception e) {
            Log.d("Aquamark", "Error in " + elementName);
            e.printStackTrace();
            return result;
        }
    }

    public boolean enterEvent(UiDevice device, String elementID, String idType, String enterText, String elementName, String successMsg) {
        boolean result = false;
        try {
            if (idType.equalsIgnoreCase("resourceID")) {
                selector = new UiSelector().resourceId(elementID);
            }
            if (idType.equalsIgnoreCase("textContains")) {
                selector = new UiSelector().textContains(elementID);
            }
            if (idType.equalsIgnoreCase("text")) {
                selector = new UiSelector().text(elementID);
            }
            if (idType.equalsIgnoreCase("description")) {
                selector = new UiSelector().description(elementID);
            }
            if (idType.equalsIgnoreCase("descriptionContains")) {
                selector = new UiSelector().descriptionContains(elementID);
            }
            return enterText(device,selector,enterText,elementName,successMsg);
        } catch (Exception e) {
            Log.d("Aquamark", "Error in " + elementName);
            e.printStackTrace();
            return result;
        }
    }
    public boolean enterText(UiDevice device,UiSelector selector,String enterText,String elementName,String successMsg){
        boolean result = false;
        try {
            Log.d("Aquamark", "Trying To Execute " + elementName);
            idObject = device.findObject(selector);
            if (idObject.waitForExists(OneMin_Timeout)) {
                idObject.click();
                idObject.setText(enterText);
                device.pressEnter();
                timestamp = new Timestamp(new Date().getTime());
                Log.d("Aquamark", successMsg + timestamp);
                result = true;
            }
            return result;
        } catch (Exception e) {
            Log.d("Aquamark", "Error in " + elementName);
            e.printStackTrace();
            return result;
        }
    }

}
