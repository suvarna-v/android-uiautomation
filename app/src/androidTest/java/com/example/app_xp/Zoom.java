package com.example.app_xp;

import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;

import com.example.app_xp.app.BaseApp;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Zoom extends BaseApp {
    public Zoom() {
        super(Zoom.class);
    }

    String appPackageName = "us.zoom.videomeetings";
    String warningOkButtonID = "us.zoom.videomeetings:id/rooted_warning_dialog_continue_btn";
    String homePageID = "us.zoom.videomeetings:id/btnStart";
    String enterMeetingID = "us.zoom.videomeetings:id/edtConfNumber";
    String enterMeetingText = "971 8885 2595";
    String joinButtonID = "us.zoom.videomeetings:id/btnJoin";
    String enterPasswordID = "us.zoom.videomeetings:id/edtPassword";
    String enterPasswordText = "8Qku8n";
    String okButtonID = "us.zoom.videomeetings:id/button1";
    String loaderID = "us.zoom.videomeetings:id/txtConnecting";
    String endMeetingID = "us.zoom.videomeetings:id/btnLeave";
    String confirmEndMeetingID = "us.zoom.videomeetings:id/btnLeaveMeeting";
    long OneMin_Timeout = 60000;
    long ThreeMin_Timeout = 180000;
    boolean value = false;

    @Before
    public void zoomLaunchingTheApplication() {
        super.startApplication();
        try {
            launchApp(appPackageName);
            captureEvent("appLaunch", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void zoomJoinPreHostedMeetingForOneMinuteIndia(){
        try {
            assertEquals("Warning Ok Button Failed", true, zoomCheckingForWarningOkButtonAndClickOnWarningOkButton());
            assertEquals("Home Elements Appearance Failed", true, zoomCheckingForTheHomeElementsAppearanceForOneMinute());
            assertEquals("Join Meeting Button Failed", true, zoomCheckingForJoinButtonAndClickOnJoinButton());
            assertEquals("Enter Meeting Details Failed", true, zoomCheckingForInputMeetingElementAndInputMeetingId());
            assertEquals("Start Meeting Button Failed", true, zoomCheckingForStartMeetingButtonAndClickOnStartMeetingButton());
            assertEquals("Enter Password Failed", true, zoomCheckingForInputPasswordElementAndInputPassword());
            assertEquals("Click Ok Button Failed", true, zoomCheckingForOkButtonAndClickOnOkButton());
            assertEquals("Run Meeting for 3mins Failed", true, zoomCheckingForTheLoaderAppearanceForOneMinute());
            assertEquals("End Meeting Button Failed", true, zoomCheckingForEndMeetingAndClickOnEndMeetingButton());
            assertEquals("Confirm End Meeting Button Failed", true, zoomCheckingForConfirmEndMeetingButtonAndClickOnConfirmEndMeeting());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void zoomJoinPreHostedMeetingForThreeMinuteIndia(){
        try {
            assertEquals("Warning Ok Button Failed", true, zoomCheckingForWarningOkButtonAndClickOnWarningOkButton());
            assertEquals("Home Elements Appearance Failed", true, zoomCheckingForTheHomeElementsAppearanceForOneMinute());
            assertEquals("Join Meeting Button Failed", true, zoomCheckingForJoinButtonAndClickOnJoinButton());
            assertEquals("Enter Meeting Details Failed", true, zoomCheckingForInputMeetingElementAndInputMeetingId());
            assertEquals("Start Meeting Button Failed", true, zoomCheckingForStartMeetingButtonAndClickOnStartMeetingButton());
            assertEquals("Enter Password Failed", true, zoomCheckingForInputPasswordElementAndInputPassword());
            assertEquals("Click Ok Button Failed", true, zoomCheckingForOkButtonAndClickOnOkButton());
            assertEquals("Run Meeting for 3mins Failed", true, zoomCheckingForTheLoaderAppearanceForThreeMinute());
            assertEquals("End Meeting Button Failed", true, zoomCheckingForEndMeetingAndClickOnEndMeetingButton());
            assertEquals("Confirm End Meeting Button Failed", true, zoomCheckingForConfirmEndMeetingButtonAndClickOnConfirmEndMeeting());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean zoomCheckingForWarningOkButtonAndClickOnWarningOkButton() {
        try {
            UiSelector selector = new UiSelector().resourceId(warningOkButtonID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnWarningOk", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnWarningOk", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForTheHomeElementsAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().resourceId(homePageID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("homeElementsAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("homeElementsAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForJoinButtonAndClickOnJoinButton() {
        try {
            UiSelector selector = new UiSelector().resourceId(joinButtonID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnJoin", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnJoin", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForInputMeetingElementAndInputMeetingId() {
        try {
            UiSelector selector = new UiSelector().resourceId(enterMeetingID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.setText(enterMeetingText);
                device.pressEnter();
                captureEvent("enterMeeting", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("enterMeeting", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForStartMeetingButtonAndClickOnStartMeetingButton() {
        try {
            UiSelector selector = new UiSelector().resourceId(joinButtonID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnStartMeeting", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnStartMeeting", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForInputPasswordElementAndInputPassword() {
        try {
            UiSelector selector = new UiSelector().resourceId(enterPasswordID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.setText(enterPasswordText);
                device.pressEnter();
                captureEvent("enterPassword", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("enterPassword", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForOkButtonAndClickOnOkButton() {
        try {
            UiSelector selector = new UiSelector().resourceId(okButtonID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnOk", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnOk", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForTheLoaderAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().resourceId(loaderID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("loaderAppear", "Yes");
                } else {
                    captureEvent("loaderAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForTheLoaderAppearanceForThreeMinute() {
        try {
            UiSelector selector = new UiSelector().resourceId(loaderID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= ThreeMin_Timeout) {
                if (object.exists()) {
                    captureEvent("loaderAppear", "Yes");
                } else {
                    captureEvent("loaderAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForEndMeetingAndClickOnEndMeetingButton() {
        try {
            UiSelector selector = new UiSelector().resourceId(endMeetingID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnEndMeeting", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnEndMeeting", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean zoomCheckingForConfirmEndMeetingButtonAndClickOnConfirmEndMeeting() {
        try {
            UiSelector selector = new UiSelector().resourceId(confirmEndMeetingID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnConfirmEndMeeting", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnConfirmEndMeeting", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    @After
    public void zoomClosingTheApplication() {
        super.closeApplication();
        try {
            closeApp(appPackageName);
            captureEvent("appKilled", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

