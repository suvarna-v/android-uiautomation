package com.example.app_xp;

import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import com.example.app_xp.app.BaseApp;
import com.example.app_xp.interfaces.UpdateDeviceStatus;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Prime extends BaseApp {
    String appPackageName = "com.amazon.avod.thirdpartyclient";
    long OneMin_Timeout = 60000;
    boolean value = false;
    String homeElementID = "TV Shows";
    String searchButtonID = "Find";
    String enterSearchID = "com.amazon.avod.thirdpartyclient:id/find_search_box_input";
    String enterSearchText = "Kung fu panda the paws of destiny \n ";
    String videoButtonID = "com.amazon.avod.thirdpartyclient:id/myStuffItemRoot";
    String playButtonID = "com.amazon.avod.thirdpartyclient:id/detail_page_header_play_button";
    String scrollID = "com.amazon.avod.thirdpartyclient:id/detail_page_scroll_root";
    String loaderID = "com.amazon.avod.thirdpartyclient:id/LoadingSpinner";

    public Prime() {
        super(Prime.class);
    }

    @Before
    @UpdateDeviceStatus(deviceId = "12212", ipAddress = "122.122.133.44", deviceStatus = "Running")
    public void primeLaunchingTheApplication() {
        super.startApplication();
        try {
            launchApp(appPackageName);
            captureEvent("appLaunch", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void primeJourneyScriptForPlaySearchedVideoForOneMinuteIndia(){
        try {
            assertEquals("Home Element Appear Failed", true, primeCheckingHomeElementsAppearanceForOneMinute());
            assertEquals("Search Button Failed", true, primeCheckingForSearchButtonAndClickOnSearchButton());
            assertEquals("Enter Search Failed", true, primeCheckingForInputSearchElementAndInputSearchTextAsKungFuPandaThePawsOfDestiny());
            assertEquals("Select Video Failed", true, primeCheckingForVideoButtonAndSelectVideo());
            assertEquals("Play Button Failed", true, primeCheckingForPlayButtonAndClickOnPlayButton());
            assertEquals("Video Play Failed", true, primeCheckingForTheLoaderAppearanceForOneMinute());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean primeCheckingHomeElementsAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().text(homeElementID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists() && object.isEnabled()) {
                    captureEvent("homePageAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("homePageAppear", "No");
                }
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private boolean primeCheckingForSearchButtonAndClickOnSearchButton() {
        try {
            UiSelector searchId = new UiSelector().text(searchButtonID);
            UiObject searchAppear = device.findObject(searchId);
            if (searchAppear.waitForExists(OneMin_Timeout)) {
                searchAppear.click();
                captureEvent("clickOnSearch", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnSearch", "Failed");
           e.printStackTrace();
        }
        return value;
    }

    private boolean primeCheckingForInputSearchElementAndInputSearchTextAsKungFuPandaThePawsOfDestiny() {
        try {
            UiSelector enterSearchField = new UiSelector().resourceId(enterSearchID);
            UiObject enterSearch_text = device.findObject(enterSearchField);

            if (enterSearch_text.waitForExists(OneMin_Timeout)) {
                enterSearch_text.click();
                enterSearch_text.setText(enterSearchText);
                device.pressEnter();
                captureEvent("enterSearchText", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("enterSearchText", "Failed");
           e.printStackTrace();
        }
        return value;
    }

    private boolean primeCheckingForVideoButtonAndSelectVideo() {
        try {

            UiCollection collection = new UiCollection(new UiSelector().resourceId(videoButtonID));
            UiObject object = collection.getChildByInstance(new UiSelector(), 0);
            if (object.waitForExists(OneMin_Timeout)) {
                object.click();
                captureEvent("selectedVideo", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("selectedVideo", "Failed");
          e.printStackTrace();
        }
        return value;
    }

    private boolean primeCheckingForPlayButtonAndClickOnPlayButton() {
        try {

            UiSelector selector = new UiSelector().resourceId(playButtonID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.click();
                captureEvent("clickOnPlay", "Success");
                value = true;
            } else {
                primeCheckingForAlternatePlayButtonAndClickOnAlternatePlayButton();
            }
        } catch (Exception e) {
            captureEvent("clickOnPlay", "Failed");
           e.printStackTrace();
        }
        return value;
    }

    private boolean primeCheckingForAlternatePlayButtonAndClickOnAlternatePlayButton() {
        try {
            UiSelector selector = new UiSelector().resourceId(playButtonID);
            UiObject object = device.findObject(selector);
            UiScrollable scroll = new UiScrollable(new UiSelector().resourceId(scrollID));
            scroll.scrollIntoView(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.click();
                captureEvent("clickOnPlay", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnPlay", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean primeCheckingForTheLoaderAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().resourceId(loaderID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("loaderAppear", "Yes");
                } else {
                    captureEvent("loaderAppear", "No");
                }

                value = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    @After
    @UpdateDeviceStatus(deviceId = "", ipAddress = "", deviceStatus = "")
    public void primeClosingTheApplication() {
        super.closeApplication();
        try {
            closeApp(appPackageName);
            captureEvent("appKilled", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
