package com.example.app_xp;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import com.example.app_xp.app.BaseApp;
import com.example.app_xp.interfaces.UpdateDeviceStatus;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Hotstar extends BaseApp {

    String appPackageName = "in.startv.hotstar";
    String searchButtonID = "in.startv.hotstar:id/action_search";
    String enterSearchID = "in.startv.hotstar:id/search_text";
    String searchText = "Koffee With Karan \n";
    String selectVideoID = "in.startv.hotstar:id/grid_content_list";
    String playButtonID = "in.startv.hotstar:id/play";
    String loaderID = "in.startv.hotstar:id/spinner";
    long OneMin_Timeout = 60000;
    boolean value = false;
    int instance = 5;

    public Hotstar() {
        super(Hotstar.class);
    }

    @Before
    @UpdateDeviceStatus(deviceId = "12212", ipAddress = "122.122.133.44", deviceStatus = "Running")
    public void hotstarLaunchingTheApplication() {
        super.startApplication();
        try {
            launchApp(appPackageName);
            captureEvent("appLaunch", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void hotstarJourneyScriptForPlaySearchedVideoForOneMinuteIndia(){
        try {
            assertEquals("Search Button Failed", true, hotstarCheckingForSearchButtonAndClickOnSearchButton());
            assertEquals("Enter Search Failed", true, hotstarCheckingForInputSearchElementAndInputSearchTextAsKoffeeWithKaran());
            assertEquals("Select Video Failed", true, hotstarCheckingForVideoOnGridAndSelectVideoWithCollection());
            assertEquals("Play Button Failed", true, hotstarCheckingForPlayButtonAndClickOnPlayButton());
            assertEquals("Video Play Failed", true, hotstarCheckingForTheLoaderAppearanceForOneMinute());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean hotstarCheckingForSearchButtonAndClickOnSearchButton() {
        try {
            UiSelector searchId = new UiSelector().resourceId(searchButtonID);
            UiObject searchAppear = device.findObject(searchId);
            if (searchAppear.waitForExists(OneMin_Timeout)) {
                searchAppear.click();
                captureEvent("clickOnSearch", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnSearch", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    private boolean hotstarCheckingForInputSearchElementAndInputSearchTextAsKoffeeWithKaran() {
        try {
            UiSelector enterSearchField = new UiSelector().resourceId(enterSearchID);
            UiObject enterSearch_text = device.findObject(enterSearchField);

            if (enterSearch_text.waitForExists(OneMin_Timeout)) {
                enterSearch_text.setText(searchText);
                captureEvent("enterSearchText", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("enterSearchText", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    private boolean hotstarCheckingForVideoOnGridAndSelectVideoWithCollection() {
        try {
            UiCollection selectVideoId = new UiCollection(new UiSelector().resourceId(selectVideoID));
            UiObject selectVideo = selectVideoId.getChildByInstance(new UiSelector(), instance);
            if (selectVideo.waitForExists(OneMin_Timeout)) {
                selectVideo.click();
                captureEvent("selectVideo", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("selectVideo", "Failed");
           e.printStackTrace();
        }
        return value;
    }

    private boolean hotstarCheckingForPlayButtonAndClickOnPlayButton() {
        try {
            UiSelector playButtonId = new UiSelector().resourceId(playButtonID);
            UiObject playButton = device.findObject(playButtonId);
            if (playButton.waitForExists(OneMin_Timeout)) {
                playButton.click();
                captureEvent("clickOnPlay", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnPlay", "Failed");
           e.printStackTrace();
        }
        return value;
    }

    private boolean hotstarCheckingForTheLoaderAppearanceForOneMinute() {
        try {
            UiSelector loaderId = new UiSelector().resourceId(loaderID);
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (loader.exists()) {
                    captureEvent("loaderAppear", "Yes");
                } else {
                    captureEvent("loaderAppear", "No");
                }

                value = true;
            }
        } catch (Exception e) {
          e.printStackTrace();
        }
        return value;
    }

    @After
    @UpdateDeviceStatus(deviceId = "", ipAddress = "", deviceStatus = "")
    public void hotstarClosingTheApplication() {
        super.closeApplication();
        try {
            closeApp(appPackageName);
            captureEvent("appKilled", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
