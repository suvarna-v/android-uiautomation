package com.example.app_xp;

import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import com.example.app_xp.app.BaseApp;
import com.example.app_xp.interfaces.UpdateDeviceStatus;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Youtube extends BaseApp {

    String appPackageName = "com.google.android.youtube";
    String searchButtonID = "Search";
    String resultsID = "com.google.android.youtube:id/results";
    String homeButtonForYoutubeLive = "Navigate up";
    String enterSearchID = "com.google.android.youtube:id/search_edit_text";
    String searchText = "coldplay concert full videos \n ";
    String searchTextForLiveVideoIndia = "Aaj Tak Live \n ";
    String searchTextForLiveVideoFrance = "france 24 live \n ";
    String playButtonIDForViews = "views";
    String playButtonIDForWatching = "Watching";
    String loaderID = "com.google.android.youtube:id/player_loading_view_thin";
    long OneMin_Timeout = 60000;
    boolean value = false;

    public Youtube() {
        super(Youtube.class);
    }

    @Before
    @UpdateDeviceStatus(deviceId = "12212", ipAddress = "122.122.133.44", deviceStatus = "Running")
    public void youTubeLaunchingTheApplication() {
        super.startApplication();
        try {
            launchApp(appPackageName);
            captureEvent("appLaunch", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void youTubeJourneyScriptForPlaySearchedVideoForOneMinuteIndia(){
        try {
            assertEquals("Search Button Failed", true, youTubeCheckingForSearchButtonAndClickOnSearchButton());
            assertEquals("Enter Search Failed", true, youTubeCheckingForInputSearchElementAndInputSearchTextAsColdPlayConcertFullConcert());
            assertEquals("Play Button Failed", true, youTubeCheckingForPlayButtonAndClickOnViewsToPlayVideo());
            assertEquals("Video Play Failed", true, youTubeCheckingForTheLoaderAppearanceForOneMinute());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void youTubeJourneyScriptForPlaySearchedLiveVideoForOneMinuteIndia(){
        try {
            assertEquals("Search Functionality Failed",true, youTubeSearchFunctionalityMethod());
            assertEquals("Home Button Appear Failed", true, youTubeCheckingForHomeButtonAndClickOnHomeButton());
            assertEquals("Home Element Appear Failed", true, youTubeCheckingHomePageAppearanceForOneMinute());
            assertEquals("Search Button Failed", true, youTubeCheckingForSearchButtonAndClickOnSearchButton());
            assertEquals("Enter Search Failed", true, youTubeCheckingForInputSearchElementAndInputSearchTextAsAajTakLive());
            assertEquals("Play Button Failed", true, youTubeCheckingForPlayButtonAndClickOnWatchingToPlayVideo());
            assertEquals("Video Play Failed", true, youTubeCheckingForTheLoaderAppearanceForOneMinute());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void youTubeJourneyScriptForPlaySearchedLiveVideoForOneMinuteFrance(){
        try {
            assertEquals("Search Functionality Failed",true, youTubeSearchFunctionalityMethod());
            assertEquals("Home Button Appear Failed", true, youTubeCheckingForHomeButtonAndClickOnHomeButton());
            assertEquals("Home Element Appear Failed", true, youTubeCheckingHomePageAppearanceForOneMinute());
            assertEquals("Search Button Failed", true, youTubeCheckingForSearchButtonAndClickOnSearchButton());
            assertEquals("Enter Search Failed", true, youTubeCheckingForInputSearchElementAndInputSearchTextAsFrance24Live());
            assertEquals("Play Button Failed", true, youTubeCheckingForPlayButtonAndClickOnWatchingToPlayVideo());
            assertEquals("Video Play Failed", true, youTubeCheckingForTheLoaderAppearanceForOneMinute());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean youTubeSearchFunctionalityMethod(){
        try {
            assertEquals("Search Button Failed", true, youTubeCheckingForSearchButtonAndClickOnSearchButton());
            assertEquals("Enter Search Failed", true, youTubeCheckingForInputSearchElementAndInputSearchTextAsColdPlayConcertFullConcert());
            assertEquals("Search Results Appear Failed", true, youTubeCheckingForSearchResultsAppearanceForOneMinute());
            value = true;
        }catch (Exception e){
            captureEvent("searchFunctionality","Failed");
        }
        return value;
    }

    public boolean youTubeCheckingForSearchButtonAndClickOnSearchButton() {
        try {
            UiSelector selector = new UiSelector().descriptionContains(searchButtonID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnSearch", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnSearch", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingForPlayButtonAndClickOnViewsToPlayVideo() {
        try {
            UiSelector selector = new UiSelector().descriptionContains(playButtonIDForViews);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnPlay", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnPlay", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingForTheLoaderAppearanceForOneMinute() {
        try {
            UiSelector loaderId = new UiSelector().resourceId(loaderID);
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (loader.exists()) {
                    captureEvent("loaderAppear", "Yes");
                } else {
                    captureEvent("loaderAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingForInputSearchElementAndInputSearchTextAsColdPlayConcertFullConcert() {
        try {
            UiSelector selector = new UiSelector().resourceId(enterSearchID);
            UiObject object = device.findObject(selector);
            if (object.waitForExists(OneMin_Timeout)) {
                object.setText(searchText);
                device.pressEnter();
                captureEvent("enterSearch", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("enterSearch", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingForSearchResultsAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().resourceId(resultsID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists() && object.isEnabled()) {
                    captureEvent("searchResultsAppear", "Yes");
                    break;
                } else {
                    captureEvent("searchResultsAppear", "No");
                }
            }
            return true;

        } catch (Exception e) {
           e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingForHomeButtonAndClickOnHomeButton() {
        try {
            UiSelector selector = new UiSelector().descriptionContains(homeButtonForYoutubeLive);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnHomeButton", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnHomeButton", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingHomePageAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().resourceId(resultsID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists() && object.isEnabled()) {
                    captureEvent("homePageAppear", "Yes");
                    break;
                } else {
                    captureEvent("homePageAppear", "No");
                }
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingForInputSearchElementAndInputSearchTextAsAajTakLive() {
        try {
            UiSelector enterSearchField = new UiSelector().resourceId(enterSearchID);
            UiObject enterSearch_text = device.findObject(enterSearchField);

            if (enterSearch_text.waitForExists(OneMin_Timeout)) {
                enterSearch_text.setText(searchTextForLiveVideoIndia);
                device.pressEnter();
                captureEvent("enterSearch", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("enterSearch", "Failed");
          e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingForInputSearchElementAndInputSearchTextAsFrance24Live() {
        try {
            UiSelector enterSearchField = new UiSelector().resourceId(enterSearchID);
            UiObject enterSearch_text = device.findObject(enterSearchField);

            if (enterSearch_text.waitForExists(OneMin_Timeout)) {
                enterSearch_text.setText(searchTextForLiveVideoFrance);
                device.pressEnter();
                captureEvent("enterSearch", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("enterSearch", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean youTubeCheckingForPlayButtonAndClickOnWatchingToPlayVideo() {
        try {
            UiSelector selector = new UiSelector().descriptionContains(playButtonIDForWatching);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnPlay", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnPlay", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    @After
    @UpdateDeviceStatus(deviceId = "", ipAddress = "", deviceStatus = "")
    public void youTubeClosingTheApplication() {
        super.closeApplication();
        try {
            closeApp(appPackageName);
            captureEvent("appKilled", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
