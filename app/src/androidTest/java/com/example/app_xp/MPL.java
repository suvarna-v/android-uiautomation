package com.example.app_xp;

import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;
import com.example.app_xp.app.BaseApp;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MPL extends BaseApp {
    String appPackageName = "com.mpl.androidapp";
    long OneMin_Timeout = 60000;
    boolean value = false;

    String spinID = "Spin the Wheel!";
    String okayID = "Okay";

    String homePageID = "All Games";
    String walletIconID = "";
    String cashBalanceID = "Cash Balance";
    String addCashButtonID = "Add Cash";
    String addDepositCashID = "Add Deposit Cash";
    String proceedToAddcashID = "Proceed To Add Cash";
    String paymentOptionID = "Payment Options";
    String chooseYourBankID = "Choose Your Bank";
    String allBanksID = "ALL BANKS";
    String iciciBankID = "ICICI Netbanking";
    String welcomePageID = "Welcome to Razorpay";
    String successButtonID = "Success";
    String paymentSuccessID = "Payment Successful";

    String selectGameID = "Ice Jump";
    String scrollViewID = "android.widget.ScrollView";
    String gameCardID = "Mozark Free Tournament";
    String playID = "Play";
    String gameOverID = "Calculating your rank";
    String leaderBoardID = "LEADERBOARD";

    public MPL() {
        super(MPL.class);
    }

    @Before
    public void mplLaunchingTheApplication() {
        super.startApplication();
        try {
            launchApp(appPackageName);
            captureEvent("appLaunch", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void mplAddMoneyToTheWalletForPreLoggedInUser() {
        try {
            assertEquals("Home Element Appear Failed", true, mplCheckingForHomePageElementsToAppearForOneMinute());
            assertEquals("Wallet Button Failed", true, mplCheckingForWalletIconAppearanceAndClickOnWalletIcon());
            assertEquals("Cash Balance Appear Failed", true, mplCheckingForCashBalanceTextAppearanceForOneMinute());
            assertEquals("Add Cash Button Failed", true, mplCheckingForAddCashButtonAppearanceAndClickOnAddCashButton());
            assertEquals("Add Deposit Cash Appear Failed", true, mplCheckingForAddDepositCashTextAppearanceForOneMinute());
            assertEquals("Proceed To Add Cash Button Failed", true, mplCheckingForProceedToAddCashAppearanceAndClickOnProceedToAddCash());
            assertEquals("Payment Options Appear Failed", true, mplCheckingForPaymentOptionsAppearanceForOneMinute());
            assertEquals("Choose Your Bank Button Failed", true, mplCheckingForChooseYourBankToAppearAndClickOnChooseYourBank());
            assertEquals("All Banks Appear Failed", true, mplCheckingForAllBanksAppearanceForOneMinute());
            assertEquals("ICICI Bank Appear Failed", true, mplCheckingForICICIBankAppearanceAndSelectICICIBank());
            assertEquals("Razorpay Page Appear Failed", true, mplCheckingForRazorPayPageAppearanceForOneMinute());
            assertEquals("Payment Success Button Failed", true, mplCheckingForSuccessButtonAppearanceAndClickOnSuccess());
            assertEquals("Payment Successful Appear Failed", true, mplCheckingForPaymentSuccessfulMessageAppearanceForOneMinute());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void mplPlayIceJumpTournamentHostedOnMozarkFreeTournamentCard() {
        try {
            assertEquals("Home Element Appear Failed", true, mplCheckingForHomePageElementsToAppearForOneMinute());
            assertEquals("Select Ice Jump Game Failed", true, mplCheckingForIceJumpGameAndClickOnIceJumpGame());
            assertEquals("Game Card Appear Failed", true, mplCheckingForGameCardAppearanceForOneMinute());
            assertEquals("Click Game Card Failed", true, mplCheckingForGameCardAppearanceAndClickOnGameCard());
            assertEquals("Game Play Button Failed", true, mplCheckingForPlayButtonAppearanceAndClickOnPlayButton());
            assertEquals("Game Over Appear Failed", true, mplCheckingForGameOverTextAppearanceForOneMinute());
            assertEquals("LeaderBoard Appear Failed", true, mplCheckingForLeaderBoardAppearanceForOneMinute());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean mplCheckingForHomePageElementsToAppearForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(homePageID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("homeElementsAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("homeElementsAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForIceJumpGameAndClickOnIceJumpGame(){
        try {
            UiSelector selector = new UiSelector().textContains(selectGameID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)) {
                object.click();
                captureEvent("clickOnGame", "Success");
                value = true;
            }else {
                mplCheckingForIceJumpGameByScrollingAndClickOnIceJumpGame();
            }
        } catch (Exception e) {
            captureEvent("clickOnGame", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForIceJumpGameByScrollingAndClickOnIceJumpGame(){
        try {

            UiSelector selector = new UiSelector().textContains(selectGameID);
            UiObject object = device.findObject(selector);

            UiScrollable scroll = new UiScrollable(
                    new UiSelector().className(scrollViewID));
            scroll.scrollTextIntoView(selectGameID);

            if (object.waitForExists(OneMin_Timeout)) {
                object.click();
                captureEvent("clickOnGame", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnGame", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForGameCardAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(gameCardID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("gameCardAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("gameCardAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForGameCardAppearanceAndClickOnGameCard() {
        try {
            UiSelector selector = new UiSelector().textContains(gameCardID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnGameCard", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnGameCard", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForPlayButtonAppearanceAndClickOnPlayButton(){
        try {
            UiSelector selector = new UiSelector().text(playID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)) {
                object.click();
                captureEvent("clickOnPlay", "Success");
                value = true;
            }else {
                mplCheckingForFreeButtonAppearanceAndClickOnFreeButton();
            }
        } catch (Exception e) {
            captureEvent("clickOnPlay", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForFreeButtonAppearanceAndClickOnFreeButton(){
        try {

            UiObject object = new UiObject(new UiSelector().className("android.view.ViewGroup").instance(1).className("android.view.ViewGroup").instance(3).className("android.view.ViewGroup").index(2));

            if(object.waitForExists(OneMin_Timeout)) {
                object.click();
                captureEvent("clickOnFree", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnFree", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForGameOverTextAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(gameOverID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("gameOverAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("gameOverAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForLeaderBoardAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(leaderBoardID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("leaderBoardAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("leaderBoardAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }


    public boolean mplCheckingForWalletIconAppearanceAndClickOnWalletIcon() {
        try {
            UiSelector selector = new UiSelector().textContains(walletIconID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnWallet", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnWallet", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForCashBalanceTextAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(cashBalanceID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("cashBalanceAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("cashBalanceAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForAddCashButtonAppearanceAndClickOnAddCashButton() {
        try {
            UiSelector selector = new UiSelector().textContains(addCashButtonID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnAddCash", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnAddCash", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForAddDepositCashTextAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(addDepositCashID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("addDepositCashAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("addDepositCashAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForProceedToAddCashAppearanceAndClickOnProceedToAddCash() {
        try {
            UiSelector selector = new UiSelector().text(proceedToAddcashID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickProceedToAddCash", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickProceedToAddCash", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForPaymentOptionsAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(paymentOptionID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("paymentOptionsAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("paymentOptionsAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForChooseYourBankToAppearAndClickOnChooseYourBank() {
        try {
            UiSelector selector = new UiSelector().textContains(chooseYourBankID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnChooseYourBank", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnChooseYourBank", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForAllBanksAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(allBanksID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("allBanksAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("allBanksAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForICICIBankAppearanceAndSelectICICIBank() {
        try {
            UiSelector selector = new UiSelector().textContains(iciciBankID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnICICIBank", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnICICIBank", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForRazorPayPageAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().textContains(welcomePageID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("razorPayWelcomePageAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("razorPayWelcomePageAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForSuccessButtonAppearanceAndClickOnSuccess() {
        try {
            UiSelector selector = new UiSelector().text(successButtonID);
            UiObject object = device.findObject(selector);
            if(object.waitForExists(OneMin_Timeout)){
                object.click();
                captureEvent("clickOnSuccessButton", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnSuccessButton", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    public boolean mplCheckingForPaymentSuccessfulMessageAppearanceForOneMinute() {
        try {
            UiSelector selector = new UiSelector().text(paymentSuccessID);
            UiObject object = device.findObject(selector);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (object.exists()) {
                    captureEvent("paymentSuccessAppear", "Yes");
                    value = true;
                    break;
                } else {
                    captureEvent("paymentSuccessAppear", "No");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    @After
    public void mplClosingTheApplication() {
        super.closeApplication();
        try {
            closeApp(appPackageName);
            captureEvent("appKilled", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}