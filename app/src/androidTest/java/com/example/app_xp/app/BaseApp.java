package com.example.app_xp.app;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;
import com.example.app_xp.BaseFunction;
import com.example.app_xp.delegate.EventLoggingDelegate;
import com.example.app_xp.EventLogger;
import com.example.app_xp.interfaces.UpdateDeviceStatus;
import org.hamcrest.core.IsNull;
import org.junit.Assert;
import java.sql.Timestamp;
import java.util.Date;

public class BaseApp implements BaseFunction {

    Context context = ApplicationProvider.getApplicationContext();
    protected UiDevice device = null;
    Timestamp launchTime = null;
    Timestamp eventTime = null;
    private EventLoggingDelegate mDelegate;

    public BaseApp(Class testClass) {
        mDelegate = getDelegate(testClass,context);
    }

    @UpdateDeviceStatus
    public void startApplication() {
        mDelegate.updateDeviceStatus(true);
    }

    /**
     * @return The {@link AppCompatDelegate} being used by this Activity.
     */
    @NonNull
    public EventLoggingDelegate getDelegate(Class testClass, Context context) {
        return EventLoggingDelegate.create(testClass, context);
    }

    @UpdateDeviceStatus
    public void closeApplication() {
        mDelegate.updateDeviceStatus(false);
    }

    @Override
    public void launchApp(String appPackage) {
        Log.d("Aquamark", "Launching the App Activity");
        try {
            device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            String launcherPackage = device.getLauncherPackageName();
            Assert.assertThat(launcherPackage, IsNull.notNullValue());
            device.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), 0);

            Intent intent = context.getPackageManager().getLaunchIntentForPackage(appPackage);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
            device.hasObject(By.pkg(appPackage));
            launchTime = new Timestamp(new Date().getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closeApp(String appPackage) {
        try {
            device.executeShellCommand("am force-stop " + appPackage);
        } catch (Exception e) {
            Log.d("Aquamark", "Error in closing the app");
            e.printStackTrace();
        }
    }

    public void clickEvent(String eventName, int Wait_Timeout) {
        try {
            UiSelector eventId = new UiSelector().descriptionContains(eventName);
            UiObject eventAppear = device.findObject(eventId);
            if (eventAppear.waitForExists(Wait_Timeout)) {
                eventAppear.click();
                eventTime = new Timestamp(new Date().getTime());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*@Override
    public void latencyKpi(String eventName) {
        try {
            double timeDiff = (eventTime.getTime() - launchTime.getTime())/ 1000.0;
            Log.d("Aquamark", "Time diff = " + timeDiff);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void captureEvent(String eventName, String eventValue) {

    }

    @Override
    public void captureEvent(String eventName, EventLogger eventValue) {

    }
}
