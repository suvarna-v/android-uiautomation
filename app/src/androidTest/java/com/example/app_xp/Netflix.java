package com.example.app_xp;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.uiautomator.UiCollection;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiSelector;
import com.example.app_xp.app.BaseApp;
import com.example.app_xp.interfaces.UpdateDeviceStatus;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class Netflix extends BaseApp {
    String appPackageName = "com.netflix.mediaclient";
    long OneMin_Timeout = 60000;
    boolean value = false;
    String homeElementId = "My List";
    String searchId = "Search";
    String searchText = "turbo \n ";
    String videoId = "Turbo FAST";
    String loaderID = "com.netflix.mediaclient:id/loading_spinner";
    String enterSearchId = "android:id/search_src_text";
    String homeElement = "com.netflix.mediaclient:id/profile_avatar_img";
    String videoId1 = "com.netflix.mediaclient:id/mini_dp_play_button";
    String videoId2 = "com.netflix.mediaclient:id/video_play_icon";
    String videoId3 = "Play";

    public Netflix() {
        super(Netflix.class);
    }

    @Before
    public void netflixLaunchingTheApplication() {
        super.startApplication();
        try {
            launchApp(appPackageName);
            captureEvent("appLaunch", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void netflixJourneyScriptForPlaySearchedVideoForOneMinuteIndia(){
        try {
            assertEquals("Home Element Appear Failed", true, netflixCheckingForHomePageElementsToAppearForOneMinute());
            assertEquals("Search Button Failed", true, netflixCheckingForSearchButtonAndClickOnSearchButton());
            assertEquals("Enter Search Failed", true, netflixCheckingForInputSearchElementAndInputSearchTextAsTurboFullConcert());
            assertEquals("Select Video Failed", true, netflixCheckingForVideoAndSelectVideoTurboFAST());
            assertEquals("Play Button Failed", true, netflixCheckingForPlayButtonAndClickOnPlayButton());
            assertEquals("Video Play Failed", true, netflixCheckingForTheLoaderAppearanceForOneMinute());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean netflixCheckingForHomePageElementsToAppearForOneMinute() {
        try {
            UiSelector homeElementsID = new UiSelector().textContains(homeElementId);
            UiObject homeElements = device.findObject(homeElementsID);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (homeElements.exists()) {
                    captureEvent("homeElementAppear", "Yes");
                    break;
                } else {
                    captureEvent("homeElementAppear", "No");
                    netflixCheckingForWatcherButtonAndClickFirstWatcher();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    private boolean netflixCheckingForWatcherButtonAndClickFirstWatcher() {
        try {
            UiCollection collection = new UiCollection(new UiSelector().resourceId(homeElement));
            UiObject object = collection.getChildByInstance(new UiSelector(), 0);
            if (object.exists()) {
                object.click();
                captureEvent("clickOnWatcher", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnWatcher", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    private boolean netflixCheckingForSearchButtonAndClickOnSearchButton() {
        try {
            UiSelector searchButtonID = new UiSelector().descriptionContains(searchId);
            UiObject searchButton = device.findObject(searchButtonID);
            if (searchButton.waitForExists(OneMin_Timeout)) {
                searchButton.click();
                captureEvent("clickOnSearch", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("clickOnSearch", "Failed");
           e.printStackTrace();
        }
        return value;
    }

    private boolean netflixCheckingForInputSearchElementAndInputSearchTextAsTurboFullConcert() {
        try {
            UiSelector enterSearchField = new UiSelector().resourceId(enterSearchId);
            UiObject enterSearch_text = device.findObject(enterSearchField);

            if (enterSearch_text.waitForExists(OneMin_Timeout)) {
                enterSearch_text.setText(searchText);
                captureEvent("searchText", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("searchText", "Failed");
           e.printStackTrace();
        }
        return value;
    }

    private boolean netflixCheckingForVideoAndSelectVideoTurboFAST() {
        try {
            UiSelector videoID = new UiSelector().descriptionContains(videoId);
            UiObject selectVideo = device.findObject(videoID);
            if (selectVideo.waitForExists(OneMin_Timeout)) {
                selectVideo.click();
                captureEvent("selectVideo", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("selectVideo", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    private boolean netflixCheckingForPlayButtonAndClickOnPlayButton() {
        try {
            UiSelector playId1 = new UiSelector().resourceId(videoId1);
            UiObject playBtn1 = device.findObject(playId1);

            UiSelector playId2 = new UiSelector().resourceId(videoId2);
            UiObject playBtn2 = device.findObject(playId2);

            UiSelector playId3 = new UiSelector().text(videoId3);
            UiObject playBtn3 = device.findObject(playId3);

            if (playBtn1.waitForExists(OneMin_Timeout)) {
                playBtn1.click();
                captureEvent("playVideo", "Success");
                value = true;

            } else if (playBtn2.waitForExists(OneMin_Timeout)) {
                playBtn2.click();
                captureEvent("playVideo", "Success");
                value = true;

            } else if (playBtn3.waitForExists(OneMin_Timeout)) {
                playBtn3.click();
                captureEvent("playVideo", "Success");
                value = true;
            }
        } catch (Exception e) {
            captureEvent("playVideo", "Failed");
            e.printStackTrace();
        }
        return value;
    }

    private boolean netflixCheckingForTheLoaderAppearanceForOneMinute() {
        try {
            UiSelector loaderId = new UiSelector().resourceId(loaderID);
            UiObject loader = device.findObject(loaderId);

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            while (stopWatch.getTime() <= OneMin_Timeout) {
                if (loader.exists()) {
                    captureEvent("loaderAppear", "Yes");
                } else {
                    captureEvent("loaderAppear", "No");
                }
                value = true;
            }
        } catch (Exception e) {
           e.printStackTrace();
        }
        return value;
    }

    @After
    @UpdateDeviceStatus(deviceId = "", ipAddress = "", deviceStatus = "")
    public void netflixClosingTheApplication() {
        super.closeApplication();
        try {
            closeApp(appPackageName);
            captureEvent("appKilled", appPackageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
